# Docker Swarm põhikäsud

### Paigaldab/uuendab stacki:

    docker stack deploy --compose-file docker-compose.yml --with-registry-auth <stack name>

### Näitab jooksvaid ja ka varasemaid protsesse ning ka deploy vigu:

    docker service ps <service name>

### Sama asi, aga rohkem infot:

    docker service ps --no-trunc <service name>

### Sama info terve stacki kohta:

    docker stack ps <stack name>

### Sama asi, aga rohkem infot:

    docker stack ps --no-trunc <stack name>

### Kustutab stacki:

    docker stack rm <stack name>

### Näitab teenuse logi:

    docker service logs <service name>

### Logi koos parameetritega:

    docker service logs -f -n 10 --raw <service name>

    -f      - näitab logi reaalajas
    -n 10   - näitab viimased 10 kirjet
    --raw   - näitab kirjeid muutmata kujul ilma dockeri infota

### Näitab kõik stackid, mis Swarmis jooksevad:

    docker stack ls

### Näitab kõik containerid, mis Swarmis jooksevad:

    docker service ls

### Näitab kõiki praeguseid ja endisi konteinereid:

    docker ps -a

### Saab "jõuga" rebootida teenuse ilma image't ja compose faili uuendamata:

    docker service update --force <service name>

### Node lahkub swarmist:

    docker swarm leave

### Manager eemaldab node'i swarmist:

    docker node rm <ID>

### Näitab kõiki swarm node’e:

    docker node ls

### Näitab kogu infot ühe node’i kohta

    docker node inspect <node hostname>

### Näitab hetkel töötavate konteinerite stat’i

    docker stats

### Näitab kõikide konteinerite stat’i

    docker stats -a

### Käsu käivitamine konkreetses konteineris (tuleb käivitada vastavas node’s):

Leia õige konteineri nimi

    $ docker ps
    CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS     NAMES
    cef31683de66   traefik:v2.5.3   "/entrypoint.sh trae…"   26 seconds ago   Up 24 seconds   80/tcp    traefik_traefik.2.77slji50c35u40qlob18yaegk
    0b9ceadec7d6   traefik:v2.5.3   "/entrypoint.sh trae…"   26 seconds ago   Up 24 seconds   80/tcp    traefik_traefik.1.uic2apx5dm8fvmvx9kxnkls4p

Seejärel käivita käsk nt. echo "test". Root kasutaja õiguste jaoks lisada -u 0

    docker exec -it -u 0 traefik_traefik.2.77slji50c35u40qlob18yaegk echo "test"

### “Tühja” Ubuntu instantsi lisamine Swarmi testimiseks:

    docker pull ubuntu
    docker service create --network web -t ubuntu

### Swarmi sisse network'i loomine

    docker network create -d overlay --attachable web