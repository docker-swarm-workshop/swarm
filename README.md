# Docker Swarm workshop

## Swarmi loomine

### Loome võrgu

    docker network create --attachable swarm-test

### Manageri install

    docker compose up -d --build

### Swarmi loomine

    docker swarm init

### Kui container registry on privaatne, logime sisse

    docker login registry.gitlab.com -u kasutaja -p token

## Workeri lisamine

### Join tokeni küsimine

    docker swarm join-token worker

### Workeri lisamine

    docker swarm join --token SWMTKN-1-2btpagpqpish2ih6a8kuetw1w1w0bqupvl5vb4w59hzqmw5sfl-9oykj3w0b2csxnwsg9cueiplq 172.29.0.3:2377

### kontrollime, kas õnnestus

    docker node list

